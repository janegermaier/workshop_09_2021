# GitLabWorkShop

# Info
Example of use Gitlab for CI with testing tools, which using Gitlab as test rabbit.

## Tools
This project starts on remote server (Ubuntu-latest) some tools which testing [GitHub Pages](https://github.com/procesor2017/gitHubWorkShop)
Tools:
 - [RobotFramewok](https://robotframework.org/) using for Test Gui and APi
 - [k6 Load testing](https://k6.io/)
 - [Postman](https://www.postman.com/)
 - [Playwright](https://playwright.dev/)

## GitLab CI/CD
For building and starting tools I using GitLab CI/CD. Everything for GitLabs setup is in .gitlab-ci.yml
Example for starting Playwright in GitLab CI/CD:

```
Playwright:
    stage: FrontEnd
    image: mcr.microsoft.com/playwright:focal
    script:
        - cd ./test/Playwright
        - npm install
        - npm test
```

## Best link 
There is links what you wanna see it
 - [Choose when to job run](https://docs.gitlab.com/ee/ci/jobs/job_control.html)
 - [CI / CD expression](https://docs.gitlab.com/ee/ci/jobs/job_control.html#regular-expressions) 
 - [Keyword Reference](https://docs.gitlab.com/ee/ci/yaml/)
